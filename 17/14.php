<?php
   $rows=array(
	array('name'=>'John','age'=>34,'gender'=>'M'),
	array('name'=>'Karen','age'=>28,'gender'=>'F'),
	array('name'=>'James','age'=>20,'gender'=>'M'),
	array('name'=>'Anna','age'=>26,'gender'=>'F'),
	array('name'=>'Kathleen','age'=>31,'gender'=>'F'),
	array('name'=>'Michael','age'=>20,'gender'=>'M'),
	array('name'=>'Mark','age'=>34,'gender'=>'M'),
   );
   
   //Build Result String
   $display_string = "<table>";
   $display_string .= "<tr>";
   $display_string .= "<th>Name</th>";
   $display_string .= "<th>Age</th>";
   $display_string .= "<th>Gender</th>";
   $display_string .= "</tr>";
   
   // Insert a new row in the table for each person returned
   foreach($rows as $row){
      $display_string .= "<tr>";
      $display_string .= "<td>$row[name]</td>";
      $display_string .= "<td>$row[age]</td>";
      $display_string .= "<td>$row[gender]</td>";
      $display_string .= "</tr>";
   }
   
   $display_string .= "</table>";
   echo $display_string;
?>