<?php
$artists = array
(
array("Michael Jackson",22,18),
array("Johnny Cash",15,13),
array("Lady Gaga",5,2),
array("Elvis Presley",17,15)
);

for ($row = 0; $row < 4; $row++) {
  echo "<p><b>Row number $row</b></p>";
  echo "<ul>";
  for ($col = 0; $col < 3; $col++) {
    echo "<li>".$artists[$row][$col]."</li>";
  }
  echo "</ul>";
}

?>
