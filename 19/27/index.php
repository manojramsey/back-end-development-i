<?php
$without_base_dir=str_replace($_SERVER['DOCUMENT_ROOT'],'',str_replace('\\','/',__DIR__));  
$without_current_dir=str_replace($without_base_dir.'/','',$_SERVER['REQUEST_URI']);
$exploded=explode('/',$without_current_dir);

if(!isset($exploded[0]{0})){
	require_once('home.php');
	die();
}
switch($exploded[0]){
	case 'posts' : require_once('posts.php'); $postManager=new PostManager($exploded); break;
	case 'users' : require_once('users.php'); break;
	default: require_once('404.php');
}
