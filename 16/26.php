<?php
$movie=new stdClass;
$movie->title = "Iron man";
$movie->year = 2008;
$movie->cast=array('Robert Downey Jr.','Terrence Howard','Jeff Bridges','Shaun Toub','Gwyneth Paltrow');
$movie->link='http://www.imdb.com/title/tt0371746/';

$movieJSON = json_encode($movie);

echo $movieJSON;
?>