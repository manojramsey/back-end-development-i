<?php
class Utilis {
  // static methods and properties are defined with the static keyword.
  static public $numCars = 0;
}

// set the number of cars
Utilis::$numCars = 3;
 
// get the number of cars
echo Utilis::$numCars;