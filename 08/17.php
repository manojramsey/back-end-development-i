<?php

class Dog {
  public $name;
  public $age;
  public $breed;
  public $owner;
  
  public function eat(){
	  echo 'yum';
  }
  public function bark(){
	  echo 'woff';
  }
}

$dog1=new Dog();
$dog1->name='Lassie';
$dog1->bark();

$dog2=new Dog();
$dog2->name='Pepper';
$dog2->bark();
