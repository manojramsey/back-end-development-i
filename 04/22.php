<?php

function passByValue( $y ){
$y = 321; // function value
printf("Inside: %d<br />", $y);
}

$x = 123; // initial value
printf( "Before: %d<br />", $x );
passByValue( $x );
printf( "After: %d<br />", $x );

?>
