<?php
class MyClass{
	private $text;
	public function __get($property){
	  if (property_exists($this, $property)) {
		return $this->$property;
	  }
	}
	public function __set($property, $value){
	  if (property_exists($this, $property)) {
		$this->$property = $value;
	  }
	}
}

$class=new MyClass;
$class->text='Hello';
echo $class->text;