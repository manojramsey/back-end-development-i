<?php
class test{
  public function __call($method_name , $parameter){
    if($method_name == "overlodedFunction"){
      $count = count($parameter);
      switch($count){
      case "1":
        echo "You are passing 1 argument";
        break;
      case "2": 
        echo "You are passing 2 parameter";
        break;
      default: return false;
      }
    }else return false;
  }
  
  public static function __callStatic($name, $arguments)
    {
        echo "Calling static method '$name' "
             . implode(', ', $arguments). "\n";
    }
}
$a = new test();
$a->overlodedFunction("test");
$a->overlodedFunction("test" , "second parameter");
test::callFunction();