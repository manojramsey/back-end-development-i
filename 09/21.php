<?php
class Car {
  public function hello()
  {
    return "beep";
  }
}
class SportsCar extends Car {
  public function hello()
  {
    return "Hallo";
  }
}
$sportsCar1 = new SportsCar();
  
//Get the result of the hello method
echo $sportsCar1 -> hello();
