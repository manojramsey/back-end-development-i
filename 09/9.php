<?php
class PropertyTest{
    private $data = array();

    public function __set($name, $value){
        echo "Setting '$name' to '$value'\n";
        $this->data[$name] = $value;
    }

    public function __get($name){
        echo "Getting '$name'\n";
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }

}

$obj = new PropertyTest;

$obj->a = 1;
echo $obj->a;
