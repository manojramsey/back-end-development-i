<?php
$password='My secret password';
echo 'Original password: '.$password.'<br />';
$encrypted_password= password_hash($password, PASSWORD_BCRYPT);
echo 'Stored password: ' .$encrypted_password;

$my_attempt='Test';
echo '<hr /><b>First attempt: '. $my_attempt.'</b><br />';
$encrypted_my_attempt=password_hash($my_attempt, PASSWORD_BCRYPT);
echo 'Stored password: ' .$encrypted_my_attempt;
echo '<br />';
if(password_verify($my_attempt,$encrypted_password)) echo 'successful';
else echo 'wrong password';

$my_attempt='My secret password';
echo '<hr /><b>Second attempt: '. $my_attempt.'</b><br />';
$encrypted_my_attempt=password_hash($my_attempt, PASSWORD_BCRYPT);
echo 'Stored password: ' .$encrypted_my_attempt;
echo '<br />';
if(password_verify($my_attempt,$encrypted_password)) echo 'successful';
else echo 'wrong password';